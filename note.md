
## VUE学习笔记：

1、使用axios请求接口（引入、封装请求工具类、api统一管理、页面调用、参数header设置、数据返回）

2、Mock.js使用（https://www.fastmock.site/）
    基本语法
    生成随机图片：Random.image('200x120', Random.color());

3、vue中如何使用工具类中的方式

4、加载更多（分页）效果实现

5、ElementUI使用（https://element.eleme.cn/#/zh-CN/）

6、vue跳转页面两种方式（https://www.jb51.net/article/183611.htm）
路由及菜单配置

7、echarts使用（https://echarts.apache.org/zh/index.html）

8、全局引入 main.js

9、样式穿透scoped

10、css 中使用js中声明的变量（https://www.jianshu.com/p/ca61d11b8d09）


vue项目部署

打包 ```npm run build```，生成`dist`目录，上传到服务器使用`Nginx`代理即可。


## 计划
### 题库输出服务后台功能：

1、数据看板
- 时间线
- 各应用搜题饼图
- 出题率折线图

2、体验账号开通

3、数据导出（根据学段年级科目字段）

4、数据查询（时间段内调用量）

5、整书管理
- 书籍查询
- 新增书籍
- 接口调试

点全屏收取菜单
