import axios from '@/utils/request';

// 首页卡片
export function getCardData(params) {
  return axios.get({
    url: '/api/home/getCardData',
    data: params
  }).then((res) => {
    return res
  })
}

// 首页折线图
export function getLineData(params) {
  return axios.get({
    url: '/api/home/getLineData',
    data: params
  }).then((res) => {
    return res
  })
}