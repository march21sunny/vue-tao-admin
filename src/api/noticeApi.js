import axios from '@/utils/request';

// 顶部消息
export function getNoticeList(params) {
  return axios.get({
    url: '/api/notice/list',
    data: params
  }).then((res) => {
    return res
  })
}
