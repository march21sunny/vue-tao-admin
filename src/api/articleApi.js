import axios from '@/utils/request';

// 文章列表
export function getArticleList(params) {
  return axios.get({
    url: '/api/article/list',
    data: params
  }).then((res) => {
    return res
  })
}
